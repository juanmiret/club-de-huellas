# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'csv'
barrios = CSV.read('db/barrios_cordoba.csv')

barrios.each do |barrio|
  Zone.create(name: barrio[0])
endAdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')