class RenameCustomers < ActiveRecord::Migration
  def change
    rename_column :customers, :full_name, :nombre
  end
end
