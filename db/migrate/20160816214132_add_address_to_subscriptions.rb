class AddAddressToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :address, :string
  end
end
