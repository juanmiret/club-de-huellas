class AddPhoneToSupplier < ActiveRecord::Migration
  def change
    add_column :suppliers, :phone, :string
  end
end
