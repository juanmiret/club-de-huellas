class AddFreeShippingToProducts < ActiveRecord::Migration
  def change
    add_column :products, :free_shipping, :boolean
  end
end
