class AddReferralCodeToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :referral_code, :string
  end
end
