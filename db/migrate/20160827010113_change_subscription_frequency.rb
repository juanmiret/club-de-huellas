class ChangeSubscriptionFrequency < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :frequency
    add_column :subscriptions, :frequency, :integer
  end
end
