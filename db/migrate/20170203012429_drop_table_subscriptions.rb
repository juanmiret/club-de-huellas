class DropTableSubscriptions < ActiveRecord::Migration
  def change
    drop_table :payments
    drop_table :deliveries
    drop_table :subscription_items
    drop_table :subscriptions
  end
end
