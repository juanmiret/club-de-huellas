class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :customer, index: true, foreign_key: true
      t.string :status
      t.string :preapproval_id
      t.string :address
      t.string :subtotal
      t.string :total
      t.references :zone, index: true, foreign_key: true
      t.string :payment_method
      t.text :note
      t.string :time_preference
      t.float :latitude
      t.float :longitude
      t.string :coupon_code
      t.string :referral_code

      t.timestamps null: false
    end
  end
end
