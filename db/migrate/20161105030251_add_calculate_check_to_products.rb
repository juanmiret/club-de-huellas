class AddCalculateCheckToProducts < ActiveRecord::Migration
  def change
    add_column :products, :calculate_price, :boolean
  end
end
