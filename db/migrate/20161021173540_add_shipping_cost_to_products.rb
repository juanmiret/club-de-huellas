class AddShippingCostToProducts < ActiveRecord::Migration
  def change
    add_column :products, :shipping_cost, :integer, default: 50
  end
end
