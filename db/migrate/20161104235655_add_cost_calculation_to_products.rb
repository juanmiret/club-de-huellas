class AddCostCalculationToProducts < ActiveRecord::Migration
  def change
    add_column :products, :cost, :integer
    add_column :products, :profit_margin, :integer
  end
end
