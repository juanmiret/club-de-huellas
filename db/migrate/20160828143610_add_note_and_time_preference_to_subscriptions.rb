class AddNoteAndTimePreferenceToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :note, :text
    add_column :subscriptions, :time_preference, :string
  end
end
