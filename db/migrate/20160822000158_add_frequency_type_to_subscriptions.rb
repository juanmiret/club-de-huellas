class AddFrequencyTypeToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :frequency_type, :string
  end
end
