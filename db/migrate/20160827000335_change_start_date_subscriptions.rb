class ChangeStartDateSubscriptions < ActiveRecord::Migration
  def change
    change_column :subscriptions, :start_date, :date
  end
end
