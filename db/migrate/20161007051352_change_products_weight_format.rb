class ChangeProductsWeightFormat < ActiveRecord::Migration
  def up
    change_column :products, :weight, :decimal
  end

  def down
    change_column :products, :weight, :integer
  end
end
