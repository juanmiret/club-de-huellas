class ChangeActivatedToResumed < ActiveRecord::Migration
  def change
    rename_column :subscriptions, :activated_at, :resumed_at
  end
end
