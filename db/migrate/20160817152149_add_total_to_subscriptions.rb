class AddTotalToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :subtotal, :integer
    add_column :subscriptions, :total, :integer
  end
end
