class AddCostOfShippingToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :cost_of_shipping, :integer
  end
end
