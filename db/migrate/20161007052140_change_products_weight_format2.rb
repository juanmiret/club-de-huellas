class ChangeProductsWeightFormat2 < ActiveRecord::Migration
  def up
    change_column :products, :weight, :string
  end

  def down
    change_column :products, :weight, :decimal
  end
end
