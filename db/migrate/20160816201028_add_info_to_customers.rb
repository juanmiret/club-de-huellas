class AddInfoToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :name, :string
    add_column :customers, :phone, :string
  end
end
