class AddEndDateToDeliveries < ActiveRecord::Migration
  def change
    rename_column :deliveries, :date, :start_date
    add_column :deliveries, :end_date, :date
  end
end
