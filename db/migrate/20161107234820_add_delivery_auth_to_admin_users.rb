class AddDeliveryAuthToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :delivery_auth, :boolean
  end
end
