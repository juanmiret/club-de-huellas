class AddDateAndStatusToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :date, :date
    add_column :payments, :status, :string
    add_reference :payments, :delivery, index: true, foreign_key: true
  end
end
