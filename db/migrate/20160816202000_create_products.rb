class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string   "name"
      t.integer   "price"
      t.integer   "weight"
      t.text   "description"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false

      t.timestamps null: false
    end
  end
end
