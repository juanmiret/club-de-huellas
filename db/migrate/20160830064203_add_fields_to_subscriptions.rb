class AddFieldsToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :cancelled_at, :date
    add_column :subscriptions, :paused_at, :date
    add_column :subscriptions, :activated_at, :date
  end
end
