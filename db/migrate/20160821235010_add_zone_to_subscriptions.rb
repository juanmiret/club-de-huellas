class AddZoneToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :zone_id, :integer
  end
end
