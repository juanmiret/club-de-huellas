class AddPriceToSubscriptionItems < ActiveRecord::Migration
  def change
    add_column :subscription_items, :unit_price, :integer
    add_column :subscription_items, :total_price, :integer
  end
end
