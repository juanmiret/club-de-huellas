class AddMercadoPagoIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :mercadopago_id, :string
  end
end
