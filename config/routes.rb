Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'productos' => 'products#index', as: :products

  get 'producto/:id' => 'products#show', as: :product
  put 'producto/:id/update_stock_status' => 'products#update_stock_status', as: :product_update_stock

  get '/mi-cuenta' => 'customer_panel#panel', as: :customer_panel
  get '/panel-proveedor' => 'supplier_panel#panel', as: :supplier_panel

  get 'order_products_index' => 'orders#products_index', as: :order_products_index

  root 'start#home'

  devise_for :customers, :controllers => {registrations: 'customers/registrations', confirmations: 'customers/confirmations'}
  devise_for :suppliers, :controllers => { registrations: 'suppliers/registrations', sessions: 'suppliers/sessions' }

  resources :orders, only: [:new, :create, :destroy]

  get 'ipn' => 'orders#ipn'
  post 'ipn' => 'orders#ipn'

  get 'contacto' => 'start#contacto'
  get 'faq' => 'start#faq'

  mount Coupons::Engine => '/', as: 'coupons_engine'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
