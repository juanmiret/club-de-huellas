class OrderMailer < ApplicationMailer
  def new_order_customer(order)
    @order = order
    mail(to: @order.customer.email, subject: "PEDIDO Nº#{@order.id} REALIZADO CON ÉXITO")
  end

  def new_order_admin(order)
    @order = order
    mail(to: ['administracion@clubdehuellas.com', 'movipaq@gmail.com'], subject: "NUEVO PEDIDO Nº#{@order.id}")
  end
end
