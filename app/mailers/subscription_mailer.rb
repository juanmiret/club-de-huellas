class SubscriptionMailer < ApplicationMailer


  def new_subscription(subscription)
    @subscription = subscription
    mail(to: @subscription.customer.email, subject: '¡Ya estás suscripto en Club de Huellas!')
  end

  def update_price(subscription)
    @subscription = subscription
    mail(to: @subscription.customer.email, subject: 'Los precios de tu suscripción han cambiado')
  end

  def new_subscription_admin(subscription)
    @subscription = subscription
    mail(to: 'administracion@clubdehuellas.com', subject: "Nueva Suscripción (Nº#{@subscription.id}")
  end


end
