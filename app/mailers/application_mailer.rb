class ApplicationMailer < ActionMailer::Base
  default from: "Club de Huellas <notificaciones@clubdehuellas.com>"
  layout 'mailer'
end
