module ApplicationHelper
  def flash_class(level)
    case level
      when "success" then "ui massive green message"
      when "error" then "ui massive red message"
      when "notice" then "ui massive blue message"
      else
        "ui massive blue message"
    end
  end

  def current_class?(test_path)
    return 'active item' if request.path == test_path
    'item'
  end

  def facebook_url
    'https://www.facebook.com/clubdehuellasok/'
  end

  def instagram_url
    'https://www.instagram.com/clubdehuellas/'
  end


end
