class Product < ActiveRecord::Base
  has_many :order_items
  belongs_to :supplier
  belongs_to :pet
  belongs_to :brand
  default_scope { order('priority ASC, name DESC') }
  scope :active, -> { where(active: true) }
  scope :in_stock, -> { where(in_stock: true)}

  before_save :set_automatic_price

  mount_uploader :photo, ProductPhotoUploader

  scope :with_brand_id, lambda { |brand_ids|
    where(brand_id: [*brand_ids])
  }
  scope :with_pet_id, lambda { |pet_ids|
    where(pet_id: [*pet_ids])
  }
  scope :search_query, lambda { |query|
    return nil  if query.blank?
    where("name ILIKE ?", "%#{query}%")
  }

  paginates_per 2

  filterrific(
      default_filter_params: {},
      available_filters: [
          :search_query,
          :with_brand_id,
          :with_pet_id
      ]
  )

  def name_with_weight
    name + ' - ' + weight.to_s + ' KG'
  end

  def set_automatic_price
    if calculate_price
      self[:price] = cost.to_f + ((cost.to_f / 100) * profit_margin) + shipping_cost
    end
  end

  def set_out_of_stock!
    update_attributes(in_stock: false)
    return true
  end

  def set_in_stock!
    update_attributes(in_stock: true)
    return true
  end
end
