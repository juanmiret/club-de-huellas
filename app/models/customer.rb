class Customer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :orders

  def name
    "#{id} - #{nombre} - #{email}"
  end

  def id_and_name
    "#{id} - #{nombre}"
  end

end
