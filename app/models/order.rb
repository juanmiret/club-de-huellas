class Order < ActiveRecord::Base
  belongs_to :customer
  belongs_to :zone
  has_many :order_items, dependent: :destroy

  geocoded_by :full_address
  before_save :geocode
  before_save :update_totals

  validates :total, numericality: { greater_than: 20 }
  validates :address, presence: true

  default_scope { order('created_at DESC') }
  scope :recibidos,  -> { where(status: 'approved') }
  scope :pendientes, -> { where(status: 'pending') }
  scope :cancelados, -> { where(status: 'cancelled') }
  scope :entregados, -> { where(status: 'delivered') }

  accepts_nested_attributes_for :order_items

  def name
    "Pedido Nº#{id} (#{customer.name})"
  end

  def full_address
    "#{address}, X5000IIG Córdoba, Argentina"
  end

  def self.status_options
    [
        ['PENDIENTE', 'pending'],
        ['RECIBIDO', 'approved'],
        ['CANCELADO', 'cancelled'],
        ['ENTREGADO', 'delivered'],
        ['PAGO DEVUELTO', 'refunded']
    ]
  end

  def self.time_preference_options
    [
        ['Cualquier Hora', 'Cualquier Hora'],
        ['Por la Mañana', 'Por la Mañana'],
        ['Por la Tarde', 'Por la Tarde']
    ]
  end

  def self.payment_method_options
    [
        ['EFECTIVO', 'EFECTIVO'],
        ['MERCADOPAGO', 'MERCADOPAGO']
    ]
  end

  def subtotal
    if persisted?
      self[:subtotal]
    else
      order_items.collect { |oi| oi.valid? ? oi.total_price : 0 }.sum
    end
  end

  def total
    if persisted?
      self[:total]
    else
      subtotal + shipping_cost
    end
  end

  def total_with_discount
    coupon = Coupons.apply(coupon_code, amount: total)
    coupon[:total].to_f.round
  end

  def shipping_cost
    order_items.each do |oi|
      return 0 if oi.product.free_shipping
    end
    0
  end

  def mp_cost
    result = total.to_f * 0.05
    result += result * 0.21
    if payment_method == "MERCADOPAGO"
      result
    else
      0
    end
  end

  def envio_cost
    envio = 60
    order_items.each do |si|
      envio += 5 * si.quantity
    end
    envio
  end

  def cancel_order!
    case payment_method
      when 'MERCADOPAGO'
        preference = MERCADOPAGO.cancel_payment(preapproval_id)
        if preference['status'] == '200'
          update_attributes(status: 'refunded')
          return true
        else
          return false
        end
      when 'EFECTIVO'
        update_attributes(status: 'cancelled')
        return true
    end
  end

  def mark_as_delivered!
    update_attributes(status: 'delivered')
    return true
  end

  private

  def update_totals
    self[:subtotal] = subtotal
    self[:total] = total
  end


end
