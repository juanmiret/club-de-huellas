class Brand < ActiveRecord::Base
  has_many :products

  def self.options_for_select
    order('id').map { |e| [e.name, e.id] }
  end

end
