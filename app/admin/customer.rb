ActiveAdmin.register Customer, as: 'CLIENTE' do

  menu label: 'CLIENTES'

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    selectable_column
    id_column
    column "NOMBRE", :nombre, sortable: :nombre do |c|
      link_to c.nombre, admin_cliente_path(c)
    end
    column :email
    column "TELEFONO", :phone
    column "PEDIDOS" do |c|
      c.orders.count
    end
    column "EMAIL CONFIRMADO?" do |c|
      if c.confirmed?
        status_tag 'SI', :ok
      else
        status_tag 'NO', :error
      end
    end
    actions
  end

  show do
    tabs do
      tab "DATOS GENERALES" do
        columns do
          column do
            columns do
              column do
                attributes_table do
                  row :id
                  row "NOMBRE" do |c|
                    c.nombre
                  end
                  row "EMAIL" do |c|
                    c.email
                  end
                  row "TELÉFONO" do |c|
                    c.phone
                  end
                  row "EMAIL CONFIRMADO?" do |c|
                    if c.confirmed?
                      status_tag 'SI', :ok
                    else
                      status_tag 'NO', :error
                    end
                  end
                end
              end
            end
          end
          column span: 2 do
            columns do
              column do
                panel 'PEDIDOS' do
                  table_for resource.orders, class: 'index_table index' do
                    column "Nº PEDIDO" do |p|
                      link_to p.id, admin_pedido_path(p)
                    end
                    column "FECHA ENTREGA" do |p|
                      text_node l(p.delivery_date, format: :short).upcase
                    end
                    column "TOTAL" do |p|
                      text_node '$' + p.total
                    end
                  end
                end
              end
            end
            columns do
              column do
                panel "OTRO" do
                  text_node 'OTRO'
                end
              end
            end
          end
        end
        columns do
          column do
            active_admin_comments
          end
        end
      end
    end
  end

  filter :orders, label: 'PEDIDOS'
  filter :email
  filter :phone, label: 'TELÉFONO'


end
