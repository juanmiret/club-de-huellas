ActiveAdmin.register Product do

  duplicatable

  menu label: 'PRODUCTOS'

  scope :active, default: true
  scope :in_stock
  scope :todos do |t|
    t.all
  end



# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  batch_action :set_active do |ids|
    batch_action_collection.find(ids).each do |product|
      product.update(active: true)
    end
  end

  batch_action :set_inactive do |ids|
    batch_action_collection.find(ids).each do |product|
      product.update(active: false)
    end
  end

  batch_action :priority, form: {
      priority: :text
  } do |ids, inputs|
    batch_action_collection.find(ids).each do |product|
      product.update_attribute(:priority, inputs[:priority])
    end
    redirect_to collection_path, notice: [ids, inputs].to_s
  end

  batch_action :set_brand, form: {
      brand_id: :text
  } do |ids, inputs|
    batch_action_collection.find(ids).each do |product|
      product.update_attribute(:brand_id, inputs[:brand_id].to_i)
    end
    redirect_to collection_path, notice: [ids, inputs].to_s
  end

  batch_action :set_pet, form: {
      pet_id: :text
  } do |ids, inputs|
    batch_action_collection.find(ids).each do |product|
      product.update_attribute(:pet_id, inputs[:pet_id].to_i)
    end
    redirect_to collection_path, notice: [ids, inputs].to_s
  end

  batch_action :set_out_of_stock do |ids|
    batch_action_collection.find(ids).each do |product|
      product.update_attribute(:in_stock, false)
    end
    redirect_to collection_path, notice: [ids, 'set out of stock'].to_s
  end

  batch_action :set_in_stock do |ids|
    batch_action_collection.find(ids).each do |product|
      product.update_attribute(:in_stock, true)
    end
    redirect_to collection_path, notice: [ids, 'set in stock'].to_s
  end

  member_action :set_out_of_stock, method: :put do
    if resource.set_out_of_stock!
      flash[:success] = 'El producto fue marcado como Fuera de Stock.'
    else
      flash[:error] = 'Hubo un error al marcar el producto como Fuera de Stock.'
    end
    redirect_to :back
  end

  member_action :set_out_of_stock, method: :put do
    if resource.set_in_stock!
      flash[:success] = 'El producto fue marcado como En Stock.'
    else
      flash[:error] = 'Hubo un error al marcar el producto como En Stock.'
    end
    redirect_to :back
  end


  permit_params do
    permitted = [:free_shipping, :name, :calculate_price, :cost, :profit_margin, :price, :weight, :description, :active, :photo, :supplier, :supplier_id, :shipping_cost, :brand_id, :pet_id]
    permitted
  end


  index do
    selectable_column
    id_column
    column "FOTO" do |p|
      image_tag p.photo.url
    end
    column "NOMBRE", :name, sortable: :name do |p|
      best_in_place p, :name, as: :input, url: [:admin, p]
    end
    column "PESO", :weight, sortable: :weight do |p|
      best_in_place p, :weight, as: :input, url: [:admin, p]
    end
    column "COSTO", :cost, sortable: :cost do |p|
      best_in_place p, :cost, as: :input, url: [:admin, p]
    end
    column "MARGEN", :profit_margin, sortable: :profit_margin do |p|
      best_in_place p, :profit_margin, as: :input, url: [:admin, p]
    end
    column "ENVIO", :shipping_cost, sortable: :shipping_cost do |p|
      best_in_place p, :shipping_cost, as: :input, url: [:admin, p]
    end
    column "PRECIO VENTA", :price, sortable: :price do |p|
      best_in_place p, :price, as: :input, url: [:admin, p]
    end
    column 'PROVEEDOR', :supplier do |p|
      best_in_place p, :supplier_id, as: :select, url: [:admin,p],:collection => Supplier.all.map{|s| [s.id,s.name]}
    end
    column "ACTIVO?", :active do |p|
      best_in_place p, :active, as: :checkbox, url: admin_product_path(p)
    end
    column "EN STOCK?" do |p|
      if p.in_stock?
        status_tag 'HAY', :ok
      else
        status_tag 'NOHAY', :error
      end
    end
    column "PRIORIDAD", :priority, sortable: :priority do |p|
      best_in_place p, :priority, as: :input, url: [:admin, p]
    end
    column "ENVIO GRATIS?", :free_shipping do |p|
      best_in_place p, :free_shipping, as: :checkbox, url: admin_product_path(p)
    end
    actions dropdown: true
  end

  form do |f|
    f.semantic_errors
    inputs 'Main' do
      input :name, label: 'NOMBRE'
      input :brand, label: 'MARCA'
      input :pet, label: 'TIPO MASCOTA'
      input :cost, label: 'PRECIO DE COSTO'
      input :profit_margin, label: 'MARGEN DE GANANCIA'
      input :calculate_price, label: 'CALCULAR PRECIO AUTOMATICAMENTE?'
      input :price, label: 'PRECIO AL PÚBLICO'
      input :shipping_cost, label: 'ENVÍO'
      input :photo, label: 'FOTO'
      input :weight, label: 'PESO'
      input :description, label: 'DESCRIPCIÓN'
      input :supplier, label: 'PROVEEDOR'
      input :active, label: 'ACTIVO?'
    end
    f.actions
  end

  filter :supplier, label: 'PROVEEDOR'
  filter :name, label: 'NOMBRE'
  filter :brand, label: 'MARCA'
  filter :pet, label: 'TIPO MASCOTA'
  filter :price, label: 'PRECIO'
  filter :shipping_cost, label: 'ENVIO'
  filter :weight, label: 'PESO'
  filter :description, label: 'DESCRIPCIÓN'

end
