ActiveAdmin.register Order, as: 'PEDIDO' do


  menu label: 'PEDIDOS'

  includes :customer, :zone

  scope :recibidos, default: true
  scope :pendientes
  scope :cancelados
  scope :entregados
  scope :todos do |s|
    s.all
  end

  member_action :cancel_order, method: :put do
    if resource.cancel_order!
      flash[:success] = 'El pedido fue cancelado.'
    else
      flash[:error] = 'Hubo un error al cancelar el pedido.'
    end
    redirect_to :back
  end

  member_action :mark_as_delivered, method: :put do
    if resource.mark_as_delivered!
      flash[:success] = 'El pedido fue entregado.'
    else
      flash[:error] = 'Hubo un error al entregar el pedido.'
    end
    redirect_to :back
  end

  index do
    selectable_column
    column "ESTADO", max_width: '100px' do |s|
      div class: 'ui list' do
        div class: 'item' do
          link_to "ID: #{s.id}", admin_pedido_path(s)
        end
        div class: 'item' do
          case s.status
            when 'approved'
              status_tag 'recibido', :ok
            when 'cancelled'
              status_tag 'cancelado', :error
            when 'pending'
              status_tag 'pendiente', :warning
            when 'delivered'
              status_tag 'entregado', :ok
            when 'refunded'
              status_tag 'pago devuelto', :error
            else
              status_tag "#{s.status}", :warning
          end
        end
      end
    end
    column "CLIENTE" do |s|
      div class: 'ui large list' do
        div class: 'item' do
          i class: 'user icon'
          div class: 'content' do
            link_to s.customer.id_and_name, admin_cliente_path(s.customer)
          end
        end
        div class: 'item' do
          i class: 'phone icon'
          div class: 'content' do
            text_node s.customer.phone
          end
        end
        div class: 'item' do
          i class: 'marker icon'
          div class: 'content' do
            text_node s.address+', '+s.zone.name
          end
        end
      end
    end
    column "PRODUCTOS" do |s|
      div class: 'ui list' do
        s.order_items.map { |si|
          div class: 'item' do
            img src: "#{si.product.photo.url}", class: 'ui avatar image'
            div class: 'content' do
              div class: 'header' do
                "#{si.quantity} x #{si.product.name} (#{si.product.weight} KG)"
              end
              text_node "#{si.product.supplier.address} (#{si.product.supplier.name})" if si.product.supplier
            end
          end
        }
      end
    end
    column "PAGO" do |s|
      div class: 'ui list' do
        case s.payment_method
          when "MERCADOPAGO"
            div class: 'item' do
              div class: 'ui blue small label' do
                i class: 'dollar icon'
                text_node s.total.to_s
              end
            end
          when "EFECTIVO"
            div class: 'item' do
              div class: 'ui green small label' do
                i class: 'dollar icon'
                text_node s.total.to_s
              end
            end
        end

      end
    end
    column "FECHA ENTREGA", :delivery_date

    actions defaults: false do |e|
      div class: 'ui list' do
        li link_to "MARCAR ENTREGADO", mark_as_delivered_admin_pedido_path(e), method: :put, data: {confirm: 'Marcar como entregado?'} if e.status != 'cancelled'
        li link_to "CANCELAR", cancel_order_admin_pedido_path(e), method: :put, data: {confirm: 'Cancelar pedido?'} if e.status != 'cancelled'
      end
    end
  end

  show do
    tabs do
      tab "DATOS GENERALES" do
        columns do
          column do
            columns do
              column do
                attributes_table do
                  row :id
                  row "ESTADO" do |s|
                    case s.status
                      when 'approved'
                        status_tag 'recibido', :ok
                      when 'cancelled'
                        status_tag 'cancelado', :error
                      when 'pending'
                        status_tag 'pendiente', :warning
                      when 'delivered'
                        status_tag 'entregado', :ok
                      when 'refunded'
                        status_tag 'pago devuelto', :error
                      else
                        status_tag 'otro', :warning
                    end
                  end
                  row "CLIENTE" do |s|
                    s.customer
                  end
                  row "DIRECCION" do |s|
                    s.address
                  end
                  row "BARRIO" do |s|
                    s.zone
                  end
                  row "FECHA DE ENTREGA" do |s|
                    s.delivery_date
                  end
                  row "PREFERENCIA HORARIA" do |s|
                    s.time_preference
                  end
                  row "MEDIO DE PAGO" do |s|
                    s.payment_method.upcase
                  end
                  row "TOTAL" do |s|
                    text_node '$' + s.total.to_s
                  end
                  row "CUPÓN" do |s|
                    s.coupon_code
                  end
                  row "TOTAL CON DESCUENTO" do |s|
                    text_node '$' + s.total_with_discount.to_s
                  end
                  row "PREAPPROVAL ID" do |s|
                    s.preapproval_id
                  end
                  row "CODIGO DE REFERIDO" do |s|
                    s.referral_code
                  end
                  row "COSTO TRANSPORTE" do |s|
                    s.envio_cost
                  end
                  row "COSTO MP" do |s|
                    s.mp_cost
                  end
                end
              end
            end
          end
          column span: 2 do
            columns do
              column do
                panel 'PRODUCTOS' do
                  table_for resource.order_items, class: 'index_table index' do
                    column "FOTO" do |si|
                      image_tag si.product.photo.url
                    end
                    column "NOMBRE" do |si|
                      link_to si.product.name, admin_product_path(si.product)
                    end
                    column "PESO" do |si|
                      text_node si.product.weight.to_s + ' KG'
                    end
                    column "PRECIO UNIT." do |si|
                      text_node '$' + si.unit_price.to_s
                    end
                    column "CANT." do |si|
                      si.quantity
                    end
                    column "SUBTOTAL" do |si|
                      text_node '$' + si.total_price.to_s
                    end

                  end
                end
              end
            end
            columns do
              column do
                panel "NOTA DEL CLIENTE" do
                  text_node resource.note
                end
              end
            end
          end
        end
        columns do
          column do
            active_admin_comments
          end
        end
      end
    end
  end

  form do |f|
    f.semantic_errors
    inputs 'Main' do
      input :status, label: 'ESTADO', as: :select, collection: Order.status_options
      input :customer, label: 'CLIENTE'
      input :zone, label: 'BARRIO', as: :select, collection: Zone.all
      input :address, label: 'DIRECCION'
      input :delivery_date, label: 'FECHA DE ENTREGA'
      input :subtotal, label: 'SUBTOTAL'
      input :total, label: 'TOTAL'
      input :payment_method, label: 'METODO DE PAGO', as: :select, collection: Order.payment_method_options
      input :note, label: 'NOTA DEL CLIENTE'
      input :time_preference, label: 'PREFERENCIA HORARIA', as: :select, collection: Subscription.time_preference_options
    end
    f.actions
  end

  permit_params :customer, :zone, :status, :delivery_date, :address, :subtotal, :total, :payment_method, :note, :time_preference

  filter :customer, label: 'CLIENTE'
  filter :status, label: 'ESTADO', as: :select, collection: Order.status_options
  filter :payment_method, label: 'METODO DE PAGO', as: :select, collection: Order.payment_method_options
  filter :zone, label: 'BARRIO'
  filter :address, label: 'DIRECCION'
  filter :delivery_date, label: 'FECHA DE ENTREGA'
  filter :subtotal
  filter :total
  filter :time_preference, label: 'PREFERENCIA HORARIA', as: :select, collection: Order.time_preference_options


# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end