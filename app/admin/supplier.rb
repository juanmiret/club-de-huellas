ActiveAdmin.register Supplier do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  menu label: 'PROVEEDORES'

  permit_params :name, :address, :phone, :email, :password

  index do
    selectable_column
    id_column
    column 'NOMBRE', :name
    column 'DIRECCIÓN', :address
    column 'TELÉFONO', :phone
    actions dropdown: true
  end

  form do |f|
    f.semantic_errors
    inputs 'Main' do
      input :name, label: 'NOMBRE'
      input :address, label: 'DIRECCIÓN'
      input :phone, label: 'TELÉFONO'
      input :email, label: 'EMAIL'
      input :password, label: 'CONTRASEÑA'
    end
    f.actions
  end

  filter :products, label: 'PRODUCTO'
  filter :name, label: 'NOMBRE'
  filter :address, label: 'DIRECCIÓN'
  filter :phone, label: 'TELÉFONO'

end
