class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update_stock_status]
  before_filter :authenticate_supplier!, only: :update_stock_status
  skip_before_action :verify_authenticity_token, only: [:update_stock_status]

  def index
    @filterrific = initialize_filterrific(
        Product.active,
        params[:filterrific],
        persistence_id: false,
        select_options: {
            with_brand_id: Brand.options_for_select,
            with_pet_id: Pet.options_for_select
        }
    ) or return
    @products = @filterrific.find.page(params[:page]).order('priority ASC, name DESC').per(20)

    respond_to do |format|
      format.html
      format.js
      format.ajax { render :partial => "list" }
    end

  rescue ActiveRecord::RecordNotFound => e
    # There is an issue with the persisted param_set. Reset it.
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end

  def update_stock_status
    respond_to do |format|
      if @product.update_attribute(:in_stock, params[:product][:in_stock])
        format.json { respond_bip_ok @product }
      else
        format.json { respond_bip_error @product }
      end
    end
  end

  def show
  end

  private
  def set_product
    @product = Product.find(params[:id])
  end
end
