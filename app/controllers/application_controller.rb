class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def access_denied(exception)
    redirect_to admin_envios_path, alert: exception.message
  end

  def after_sign_in_path_for(resource)
    case resource
      when Customer
        customer_panel_path
      when Supplier
        supplier_panel_path
      else
        super
  end
  end
end
