class OrdersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:ipn]
  before_action :set_order, only: [:destroy]
  before_action :authenticate_customer!, except: [:ipn]


  require 'mercadopago.rb'


  # GET /subscriptions/new
  def new
    @last_order = current_customer.orders.first
    if @last_order
      @order = Order.new(
          zone_id: @last_order.zone_id || 239,
          payment_method: @last_order.payment_method || 'EFECTIVO',
          address: @last_order.address,
          time_preference: @last_order.time_preference || 'Cualquier Hora',
          note: @last_order.note || ''
      )
    else
      @order = Order.new(
          zone_id: 239,
          payment_method: 'EFECTIVO',
      )
    end

    if Date.today.friday?
      @order.delivery_date = Date.today.monday.next_week
    elsif Date.today.workday?
      @order.delivery_date = 1.business_day.after(Time.now).to_date
    else
      @order.delivery_date = 1.business_day.after(Time.now).to_date
    end
    @filterrific = initialize_filterrific(
        Product.active,
        params[:filterrific],
        persistence_id: false,
        select_options: {
            with_brand_id: Brand.options_for_select,
            with_pet_id: Pet.options_for_select
        }
    ) or return
    @products = @filterrific.find.page(params[:page]).order('priority ASC, name DESC').per(16)

    respond_to do |format|
      format.html
      format.js
      format.ajax { render :partial => "list" }
    end

  rescue ActiveRecord::RecordNotFound => e
    # There is an issue with the persisted param_set. Reset it.
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end


  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @order = Order.new(order_params)
    @order.customer = current_customer
    @order.status = 'pending'
    if params[:order_items]
      params[:order_items].each do |item|
        @order.order_items.new(product_id: item[:product_id].to_i, quantity: item[:quantity].to_i)
      end
    end

    redirect_path = customer_panel_path

    total_with_discount = Coupons.redeem(@order.coupon_code, amount: @order.total)
    total_with_discount = total_with_discount[:total].round


    respond_to do |format|
      if false # @order.save was the default, set to false to deativate orders
        case @order.payment_method
          when "MERCADOPAGO"
            preference_items = [{
                id: @order.id,
                title: "PEDIDO Nº#{@order.id} EN CLUB DE HUELLAS",
                quantity: 1,
                unit_price: total_with_discount,
                currency_id: 'ARS'
                                    }]
            preference_data = {
                items: preference_items,
                payer: {
                    name: @order.customer.nombre,
                    email: @order.customer.email
                },
                back_urls: {
                    success: 'http://clubdehuellas.com/mi-cuenta',
                    pending: 'http://clubdehuellas.com/mi-cuenta',
                    failure: 'http://clubdehuellas.com/mi-cuenta'
                },
                notification_url: 'http://clubdehuellas.com/ipn',
                external_reference: @order.id
            }

            preference = MERCADOPAGO.create_preference(preference_data)

            @order.preapproval_id = preference['response']['id']

            logger.debug preference
            redirect_path = preference['response']['init_point']

          when "EFECTIVO"
            @order.status = 'approved'
        end

        if @order.save
          flash[:success] = "¡El pedido fue realizado con éxito! Vas recibir tu pedido el día #{l(@order.delivery_date, format: :short)}"
          OrderMailer.new_order_admin(@order).deliver_now
          OrderMailer.new_order_customer(@order).deliver_now if @order.payment_method == 'EFECTIVO'
          format.html { redirect_to redirect_path }
        end

      else
        flash[:error] = "No podemos recibir pedidos en este momento. Disculpe las molestias. Puede comunicarse con nosotros enviando un email a administracion@clubdehuellas.com"
        format.html { redirect_to new_order_path }
      end
    end
  end

  def ipn
    case params[:topic]
      when 'payment'
        payment = MERCADOPAGO.get_payment(params[:id])
        payment = payment['response']['collection']
        @order = Order.find(payment['external_reference'].to_i)
        @order.update_attribute(:status, payment['status'])
        if payment[:status] === 'approved'
          OrderMailer.new_order_customer(@order).deliver_now
        end
      else
    end

    respond_to do |format|
      format.all { head 200, content_type: "text/html" }
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    if @order.cancel_order!
      flash[:success] = 'El pedido fue cancelado.'
    else
      flash[:error] = 'Hubo un error al cancelar el pedido, por favor intente de nuevo'
    end
    respond_to do |format|
      format.html { redirect_to customer_panel_path }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:delivery_date, :referral_code, :address, :zone_id, :note, :time_preference, :payment_method, :coupon_code, order_items_attributes: [:product_id, :quantity])
  end
end