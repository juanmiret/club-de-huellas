class CustomerPanelController < ApplicationController
  before_filter :authenticate_customer!
  before_filter :set_customer

  def panel
  end

  private
  def set_customer
    @customer = current_customer
  end
end
