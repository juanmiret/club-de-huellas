class SupplierPanelController < ApplicationController
  before_filter :authenticate_supplier!
  before_filter :set_supplier

  layout "suppliers"

  def panel
    @filterrific = initialize_filterrific(
        @products,
        params[:filterrific],
        persistence_id: false,
        select_options: {
            with_brand_id: Brand.options_for_select,
            with_pet_id: Pet.options_for_select
        }
    ) or return
    @products = @filterrific.find.page(params[:page]).order('priority ASC, name DESC').per(100)

    respond_to do |format|
      format.html
      format.js
      format.ajax { render :partial => "list" }
    end

  rescue ActiveRecord::RecordNotFound => e
    # There is an issue with the persisted param_set. Reset it.
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end

  def index
  end

  private
  def set_supplier
    @supplier = current_supplier
    @products = current_supplier.products
  end
end
