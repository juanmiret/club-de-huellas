// Define function to submit Filterrific filter form
Filterrific.submitFilterForm = function(){
    var form = $(this).parents("form"),
        url = form.attr("action");
    // turn on spinner
    $('#filterrific_dimmer').addClass('active');
    // Submit ajax request
    $.ajax({
        url: url,
        data: form.serialize(),
        type: 'GET',
        dataType: 'script'
    }).done(function( msg ) {
        $('#filterrific_dimmer').removeClass('active');
    });
};