var isMobile = window.matchMedia("only screen and (max-width: 1127px)");

$(document).ready(function () {

    if (isMobile.matches) {
        //Conditional script here
        $('.zone_select').addClass('ui fluid search dropdown').dropdown();
        $('.ui.pagination.menu').addClass('mini');
    } else {
        $('.selectize').selectize({
            allowEmptyOption: true
        });
    }

    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;

    $('#mobile_menu_sidebar').sidebar('attach events', '.toggle-mobile-menu', 'toggle');

    $('.ui.dropdown').dropdown({
        on: 'hover'
    });

    $(".best_in_place").best_in_place();

});