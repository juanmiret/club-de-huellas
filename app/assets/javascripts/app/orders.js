var calculate_order_total = function () {
    var total = 0;
    var shipping_cost = 0;
    $('#order_products .free-shipping').map(function () {
        product_free_shipping = $(this).text();
        console.log(product_free_shipping);
        if(product_free_shipping === 'true') {
            shipping_cost = 0
        }
        $('#order_products .total-price .amount').last().text('$' + shipping_cost);
    });

    $('#order_products .total-price .amount').map(function () {
        product_price = parseInt($(this).text().substr(1));
        total += product_price
    });

    $('#order_total').text('$' + total);
    console.log('calculate order total');
    return total
};

var apply_coupon = function (coupon_code) {
    var total = calculate_order_total();
    var response = $.get('/coupons/apply', {amount: total, coupon: coupon_code});
    var total_with_discount;
    response.done(function(options) {
        //=> {amount: 600.0, discount: 100.0, total: 500.0}
        total_with_discount = Math.round(options['total']);
        $('#order_total').text('$' + total_with_discount);
        return total_with_discount
    });
};

var set_remove_buttons = function () {
    $('.product-remove').click(function () {
        $target = $(this).closest('.product_row');
        $target.hide('normal', function(){
            $target.remove();
            calculate_order_total();
        });
        console.log('set remove buttons');
    });
    calculate_order_total();
};

var set_add_to_order = function () {
    $('.add_to_order').click(function () {
        var $table_body = $('#order_products');
        var $product_id = $(this).attr('data-product-id');
        var $product_name = $(this).attr('data-product-name');
        var $product_weight = $(this).attr('data-product-weight');
        var $product_price = parseInt($(this).attr('data-product-price'));
        var $product_free_shipping = $(this).attr('data-product-free-shipping');
        var $product_photo_url = $(this).attr('data-product-photo');

        var $input = $("<input>", {type: "hidden", name: "order_items[][product_id]", id: "order_items__product_id", value: $product_id});


        var $product_row = $("<tr>", {class: "product_row"});
        var $product_img_td = $("<td>", {class: "product-img"});
        var $product_photo = $("<img>", {src: $product_photo_url, class: 'ui tiny centered image'});
        var $product_title = $("<td class='product-name'>" + $product_name + "</td>");
        var $product_weight_td = $("<td class='product-weight'>" + $product_weight + " KG</td>");
        var $product_unit_price = $("<td class='unit-price'><span class='amount'>$" + $product_price + "</span></td>");
        var $product_quantity = $("<td>", {class: 'product-quantity'});
        var $quantity_area = $("<div>", {class: 'quantity-area'});
        var $cart_plus_minus = $("<div>", {class: 'ui right labeled large input'});
        var $quantity_input = $("<input type='text' value='1' name='order_items[][quantity]' id='order_items__quantity' readonly>");
        var $qty_dec = $("<a>", {class: 'decrease-btn qty-btn ui large label', text: '-'});
        var $qty_inc = $("<a>", {class: 'increase-btn qty-btn ui large label', text: '+'});
        var $product_total_price = $("<td class='total-price'><span class='amount'>$" + $product_price + "</span></td>");
        var $product_shipping_cost_hidden = $("<td class='hidden free-shipping'>" + $product_free_shipping + "</td>");

        var $remove_button = $("<td class='product-remove'><i class='big icon close'></i></td>");

        $product_img_td.append($product_photo);

        $cart_plus_minus.append($qty_dec);
        $cart_plus_minus.append($quantity_input);
        $cart_plus_minus.append($qty_inc);

        $quantity_area.append($cart_plus_minus);
        $product_quantity.append($quantity_area);

        $product_row.append($remove_button);
        $product_row.append($product_img_td);
        $product_row.append($product_title);
        $product_row.append($product_weight_td);
        $product_row.append($product_unit_price);
        $product_row.append($product_quantity);
        $product_row.append($product_total_price);
        $product_row.append($product_shipping_cost_hidden);
        $product_row.append($input);


        $table_body.prepend($product_row);

        $qty_inc.click(function () {
            input = $(this).closest('.ui.input').find('input');
            oldVal = parseInt(input.val());
            price_div = $(this).closest('.product_row').find('.total-price .amount');
            oldPrice = parseInt(price_div.text().substr(1));
            newPrice = oldPrice + $product_price;
            price_div.text('$' + newPrice);
            input.val(oldVal + 1);
            calculate_order_total();
        });

        $qty_dec.click(function () {
            input = $(this).closest('.ui.input').find('input');
            oldVal = parseInt(input.val());
            if(oldVal != 1) {
                input.val(oldVal - 1);
                price_div = $(this).closest('.product_row').find('.total-price .amount');
                oldPrice = parseInt(price_div.text().substr(1));
                newPrice = oldPrice - $product_price;
                price_div.text('$' + newPrice);
                calculate_order_total();
            }

        });


        $add_product_sidebar.deactivate();
        set_remove_buttons();

    });

    console.log('set add to order');
};

$(document).ready(function () {

    $('#apply-coupon').click(function () {
        coupon_code = $('#order_coupon_code').val();
        apply_coupon(coupon_code)
    });
});

$(document).on('ajaxp:loading', function() {
    $('#product_list_dimmer').dimmer('show');
    $('#full_page_dimmer').dimmer('show');
    window.scrollTo(0, 200);
    $('#add_product_sidebar').scrollTop(0);
    return false
});

$(document).on('ajaxp:loaded', function () {
    $('#product_list_dimmer').dimmer('hide');
    $('#full_page_dimmer').dimmer('hide');
    window.scrollTo(0, 200);
    $('#add_product_sidebar').scrollTop(0);
    if (isMobile.matches) {
        //Conditional script here
        $('.ui.pagination.menu').addClass('mini');
    }
    return false
});