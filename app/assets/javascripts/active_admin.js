//= require active_admin/base
//= require activeadmin_addons/all
//= require underscore
//= require gmaps/google
//= require semantic_ui/semantic_ui
//= require jquery.purr
//= require best_in_place
//= require best_in_place.jquery-ui
//= require best_in_place.purr

$(function () {
    $('#sidebar').detach().prependTo('#active_admin_content');
    $(".best_in_place").best_in_place();
});

